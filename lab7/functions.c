/*
Author: Keegan Shudy
Description: A collection of simple functions to be run be another function to show 
a function as a parameter
*/

#include "stdio.h"
#include "math.h"

/*
Rounds a given double to the nearest integer 
all other methods depend upon this method
*/
int roundMe(double d){
	return (d-floor(d) < 0.5 ? floor(d) : ceil(d));
} 


/*Checkes if a given number is a prime number or not
returns: 1 if prime 0 if not prime
*/
int isPrime(double d){
	double max = sqrt(d);
	int i;
	for(i = 2; i < max; i++)
		if(roundMe(d) % i == 0) return 0;
	return 1;
}
/*
Checks if a number is odd
returns: 1 if odd 0 if even
*/
int isOdd(double d){
	return (roundMe(d) % 2 == 1 ? 1 : 0);
}


/*A function runner for all other above functions*/
int calc(int (*f)(double), double d){
	return f(d);
}

int main()
{
	printf("Round 15.236 is: %d\n", calc(roundMe, 15.236));
	printf("Is 13.0 prime (1: yes 0: no)? %d\n", calc(isPrime, 13.0));
	printf("Is 22.321 odd or even (1: odd 0: even)? %d\n", calc(isOdd, 22.321));
	return 0;
}
