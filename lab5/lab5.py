"""
Keegan Shudy
CS220
Lab5
"""

#counts the occurances of x in list l
def part_1():
	print 'Enter a list separated by spaces: '
	list = raw_input()
	list = list.split()
	print 'Enter an item to find: '
	find = raw_input()

	print 'Number of %s\'s in the list %s are: %d' % (find, list, list.count(find))

#finds the intersection of two sets 
def part_2():
	print 'Enter a list separated by spaces: '
	list1 = raw_input()
	list1 = list1.split()
	print 'Enter another list separated by spaces: '
	list2 = raw_input()
	list2 = list2.split()

	print 'The intersection of the list %s, and %s is: [%s]' % (list1, list2, ','.join(set(list1) & set(list2)))

#finding the non duplicates between two lists
def part_3():
	print 'Enter a list separated by spaces: '
	list1 = raw_input()
	list1 = list1.split()
	print 'Enter another list separated by spaces: '
	list2 = raw_input()
	list2 = list2.split()

	print 'The non-duplicate list from %s, and %s is: [%s]' % (list1, list2, ','.join((set(list1) - set(list2)) | set(list2) - set(list1)))

#rotating a list by x positions to the left or -x postions to the right
def part_4():
	print 'Enter a list separated by spaces: '
	list1 = raw_input()
	list1 = list1.split()
	print 'Enter the number of positions to rotate: '
	k = input()

	print 'The list %s rotated %d times is %s' %(list1, k, (list1[k:] + list1[:k]))

def main():
	#uncomment out all the ones you want to run
	#part_1()
	#part_2()
	#part_3()
	part_4()


if  __name__ =='__main__':main()