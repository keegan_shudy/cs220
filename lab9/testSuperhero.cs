/*
Keegan Shudy
*/

using System;

class testSuperhero{

	static void Main(){
		//constructing super heros
		superhero superman = new superhero("Superman");
		superhero batman = new superhero("Batman");
		superhero flash = new superhero("Flash");
		superhero antman = new superhero("Antman");

		//assigning initial age and rank values
		superman.Age = 35;
		superman.Rank = 1;

		batman.Age = 31;
		batman.Rank = 2;

		flash.Age = 27;
		flash.Rank = 3;

		antman.Age = 24;
		antman.Rank = 3;

		//making them battle
		battle(superman, batman);
		battle(flash, antman);
		battle(superman, antman);
		Console.WriteLine("\nChange the rank of a superhero!(superman,batman,flash, antman) ex: antman 1 ? ");
		string contestent = Console.ReadLine();

		string [] contestents = contestent.Split(new []{" "}, StringSplitOptions.None);

		switch(contestents[0].ToLower()){
			case "superman":
				superman.Rank = Int32.Parse(contestents[1]);
				break;
			case "batman":
				batman.Rank = Int32.Parse(contestents[1]);
				break;
			case "flash":
				flash.Rank = Int32.Parse(contestents[1]);
				break;
			case "antman":
				antman.Rank = Int32.Parse(contestents[1]);
				break;
		}
		Console.WriteLine("\n");
		battle(superman, batman);
		battle(flash, antman);
		battle(superman, antman);


	}
	//this method compares the rank of two superheros
	//if the rank of one super hero is less than another
	//then that super hero is the winner of the battle
	public static void battle(superhero one, superhero two){
		if(one.Rank < two.Rank)
			Console.WriteLine(one.Name + ", age " + one.Age + ", rank " + one.Rank + 
				", can beat " + two.Name + ", age " + two.Age + ", rank " + two.Rank);
		else if (one.Rank > two.Rank)
			Console.WriteLine(two.Name + ", age " + two.Age + ", rank " + two.Rank + 
				", can beat " + one.Name + ", age " + one.Age + ", rank " + one.Rank);
		else
			Console.WriteLine(one.Name + ", age " + one.Age + ", rank " + one.Rank + 
				", is tied with " + two.Name + ", age " + two.Age + ", rank " + two.Rank);

	}


}