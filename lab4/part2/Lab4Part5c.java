class Lab4Part5c{

	/*
		Finds the greates common divisor of two numbers
		Using Euclid's algorithm
	*/
	private static int GCD(int first, int second){
		return second == 0 ? first : GCD(second, first % second);
	}

	public static void main(String[] args) {
		System.out.println(GCD(8, 12)); //4 expected
		System.out.println(GCD(54,24)); //6 expected
	}
}