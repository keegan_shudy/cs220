class superhero{

	//var declairation
	private int age,rank;
	private string name;
	//constructor
	public superhero(string name, int age = 0, int rank = 0){
		this.age = age;
		this.rank = rank;
		this.name = name;
	}

	//getter/setter methods
	public int Age{get; set;} 
	public int Rank{get; set;}
	public string Name{
		get{return name;}
	}
}