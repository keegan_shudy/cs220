/*
Keegan Shudy
*/

#include <string>
#include <iostream>
using namespace std;


//object for declairing a major
class Major{
	private:
		string major;
	public:
		Major(string major){
			this->major = major;
		}

		string getMajor(){
			return major;
		}

};

//object for declairing a minor
class Minor{
	private:
		string minor;
	public:
		Minor(string minor){
			this->minor = minor;
		}

		string getMinor(){
			return minor;
		}

};

//object of a person that has a major and minor that are inherited from the other classes
//and a year of the person
class person: public Major, public Minor{
	private:
		int year;
	public:
		person(string major, string minor, int year):Major(major), Minor(minor){
			this -> year = year;
		}

		int getYear(){
			return year;
		}

};

//declair and print out a person
int main(){
	person keegan("Computer Science", "Economics", 4);

	cout << "Major: " << keegan.getMajor() << "\nMinor: " << keegan.getMinor() << "\nYear: " << keegan.getYear() << "\n";
	return 0;
}
