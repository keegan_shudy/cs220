/*
Keegan Shudy
*/
#include "Gator.h"
#include <string>
using namespace std;
	
	//init globals
	string name;
	string color;
	
	//setting up constructor to take name and color
	Gator::Gator(string n, string c){
		name = n;
		color = c;
	}
	
	//setting up constructor to take just name and set default color
	Gator::Gator(string n){
		name = n;
		color = "Green";
	}
	
	//setter for name variable
	void Gator::setName(string n){
		name = n;
	}
	
	//setter for color variable
	void Gator::setColor(string c){
		color = c;
	}
	
	//getter for name variable
	string Gator::getName(){
		return name;
		
	}
	
	//getter for color variable
	string Gator::getColor(){
		return color;
	}
