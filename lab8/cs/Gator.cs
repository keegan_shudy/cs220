using System;

public class Gator
{
	private String color {get; set;}
	private String name {get; set;}
	
	
	public Gator(String color, String name){
		this.color = color;
		this.name = name;
	}
	
	public Gator(String name){
		this.name = name;
		color = "Green";
	}
}