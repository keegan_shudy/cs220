{--
Keegan Shudy
CS220 Lab6
--}
import System.Environment
 
-- | 'main' runs the main program
main :: IO ()
main = putStrLn $ "Hello there! "

add x y = x + y
sub x y = x - y
neg x = not x

--This will cut a list at a given element 
-- cutListAt 2 [1,2,3,4] -> [1,2]
cutListAt x (y:ys)
  | x /= y    = y : cutListAt x ys
  | otherwise = [y]

--Check if a string is a pallendrone
-- isPallendrone "racecar" -> true
isPallendrone s = s == reverse s

--Check if a number is prime
-- isPrime 2 -> True
isPrime :: Integer -> Bool
isPrime x = null [s | s <- [2.. x-1], x `mod` s == 0]