 Keegan Shudy

 0 bipush 10         	push in 10             
 2 istore_1				Store 10 in location 1
 3 bipush 20			push 20
 5 istore_2				Store 20 in location 2
 6 iconst_0				Load 0
 7 istore_3				Store 0 in loaction 3
 8 iload_1				Load in var 1
 9 bipush 10			push in 10
11 if_icmple 23 (+12)	Compare var 1 <= 10 if true go to 23 //skip instructions 12-22 short circuit
14 iload_2				Load in var 2
15 bipush 20			push 20 on the stack
17 if_icmpne 23 (+6)	compare var 2 != 20 if true go to 23
20 bipush 100			push 100
22 istore_3				store 100 in 3
23 iload_3				load 3
24 ireturn				return 3 done
