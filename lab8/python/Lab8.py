from Gator import *


def main():
	#init default Gators
	g1 = Gator('Keegan')
	g2 = Gator('Bob')
	
	#Display default Gators
	print "There are two default gators: " + g1.getName() + ", " + g2.getName()
	print "With default colors: {0}, {1}".format(g1.getColor(), g2.getColor())
	
	#Setup changing the color of a defined Gator
	print "To change the color of a gator, give the gator name and then the color \n (ex: Keegan blue)"
	mod = raw_input()
	mod = mod.split(" ")
	if(mod[0] == 'Keegan'):
		g1.setColor(mod[1])
		print "{0} now has color {1}" .format(g1.getName(), g1.getColor())
	else:
		g2.setColor(mod[1])
		print "{0} now has color {1}" .format(g2.getName(), g2.getColor())
		

if __name__ == "__main__":
    main()
