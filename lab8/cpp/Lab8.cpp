/*
Keegan Shudy
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<iostream>
#include "Gator.h"


using namespace std;
int main(){

	//init default Gators
	Gator g1 = Gator("Keegan");
	Gator g2 = Gator("Bob");
	
	string input1, input2;
	
	//output the default Gators
	cout << "There are two default Gators: " << g1.getName() << ", " << g2.getName() << "\n";
	cout << "They have default colors: " << g1.getColor() << ", " << g2.getColor() << "\n";
	
	//begin changing the color of a given gator
	cout << "Enter a gator to change the color: ";
	cin >> input1;
	cout <<"Enter the color to change " << input1 << " to: ";
	cin >> input2;
	
	
	if(input1 == "Keegan"){
		g1.setColor(input2);
		cout << input1 << " color: " << input2 << "\n";
	}
	else{
		g2.setColor(input2);
		cout << input1 << " color: " << input2 << "\n";
	}
}
