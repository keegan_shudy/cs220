/*
Keegan Shudy
*/
#include <string>
class Gator{
	
	//defining private variables
	private:
	    std::string name;
	    std::string color;
	    
	/*
	defining in order: setter for name, color
		           getter for name, color
		           constructor that takes name and color
		           constructor that takes name
	*/
	public:
	    void setName(std::string n);
	    void setColor(std::string c);
	    std::string getName();
	    std::string getColor();
	    Gator(std::string n, std::string c);
	    Gator(std::string name);


};
