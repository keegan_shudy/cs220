public class Lab4Part4a {
  public static void main(String[] args) {
    for (int i = 0; i <= 20; i++) {
      System.out.println("pow2("+i+") = "+pow2(i));
    }
  }

  public static int pow2(int n) {

    return n<=0 ? 1 : (2 * pow2(n-1));

  }
}
