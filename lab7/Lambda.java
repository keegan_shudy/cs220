/*
Author: Keegan Shudy
Description: a set of simple functions to show java's lambda statements as well
as parameter passing as arguements
*/

import java.util.*;
import java.util.function.*;
public class Lambda{


	public static void main(String [] args){
		
		//Take a number and multiply it by a random number whose range varies as the loop increases
		for(int i = 0; i<20; i++)
			System.out.println(mult(i, (Integer x)-> {
				return (int) ((x) * Math.random() + 1); //(High - Low * MATH_RANDOM + Low + 1)
			}));
		//Take an input string and check to see if that string is a pallendrone or not
		System.out.println(concat("Racecar", (String s) -> {
			if(s.equalsIgnoreCase(new StringBuilder(s).reverse().toString()))
				return s + " is a pallendrone";
			return s + " is not a pallendrone";
		}));
	}
	/*A function runner for integer type*/
	public static Integer mult(int r, Function<Integer, Integer> f){
		return f.apply(r);
	}
	/*A function runner for String type*/
	public static String concat(String s, Function<String, String> f){
		return f.apply(s);
	}
}
